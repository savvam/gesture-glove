## message format:

#  message_id   receiver_id   transmitter_id  command  acknowledgement
#    1_1_0_s_a_
##

# S - snap
# F - fist
# P - pinch
# l - left
# r - right
# d - down
# u - up

# import some libraries
import serial
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from math import sqrt
import random
import statistics as st
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures

#    ['ex', '11.41', 'ey', '3.84', 'ez', '-1.82', 'p1', '1', 'p2', '1', 'p3', '1', 'sys_cal', '0', 'gyro_cal', '0', 'time', '134125']

# that's where the business is at. does everything
class GestureSweeper(object):

    def __init__(self):
        self.ongoing_num_ = 0 # number of ongoing gestures
        self.gestures_ = ['point', 'fist', 'snap', 'pinch']
        self.ongoing_gestures_ = {} # gestures that we are currnetly running}
        self.start_times_ = {} #the time that the gesture started
        self.timeout_ = 5000 #break the gesture after 5 seconds
        # self.df_ =  {'point': None, 'fist': None, 'snap': None} # dataframes for storing rows of data
        self.df_ = None
        self.cmd_ = 'none'
        self.col_names_ = {'ex':[], 'ey':[], 'ez':[], 'p1':[], 'p2':[], 'p3':[], 'sys_cal':[], 'gyro_cal':[], 'time':[]}
        self.verbose_ = False

        self.processor_ = GestureProcessor()
        self.gestureList_ = GestureList()
        self.gestureGen_ = GestureGenerator()
        self.learning_ = False

        self.recipient_ = 1
        self.numRecipients_ = 2
        self.recipients_ = {1: 'wall', 2: 'screen'}

        self.last_gesture_time_ = -500 # only allow starting new gestures or adding rows after half a second has passed since the last gesture.
        self.post_gesture_delay_ = 500 # Used to eliminate accidental gestures; reducing noise
        self.reset_all()

    def learn(self):
        print("------------------------------------------------")
        print("the glove is in learning mode")
        print("------------------------------------------------")
        self.learning_ = True

    def running(self, gest):
        """
        return True if the gesture is running
        """
        return self.ongoing_gestures_[gest]

    def reset_gesture(self, gest):
        """
        reset the variables after the gesture has ended/been stopped
        """
        self.ongoing_num_ -= 1
        self.ongoing_gestures_[gest] = False
        self.start_times_[gest] = None
        # self.dfs_[gest] = None
        if gest == 'point':
            self.df_ = None
        if self.verbose_:
            print("RESET:"  + gest)
            print()

    def reset_all(self):
        """
        reset all gestures
        """
        self.ongoing_num_ = 0 # number of ongoing gestures
        self.ongoing_gestures_ = {}
        self.start_times_ = {}
        self.df_ = None
        for gest in self.gestures_:
            self.ongoing_gestures_[gest] = False
            self.start_times_[gest] = None

    def start_gesture(self, gest, row):
        """
        init all the gesture related variables, put the current row in
        """
        if self.checkTimeDelay(row['time']):
            # if gest == 'pinch': # it's pinch and pinch hasn't been started
            #     self.ongoing_gestures_['pinch'] = True
            #     self.start_times_['pinch'] = row['time']

            if gest == 'point':
                self.df_ = pd.DataFrame(self.col_names_)
                self.df_ = self.df_.append(row, ignore_index=True)

            else:
                if self.ongoing_gestures_[gest]: # hold up this gesture is already running, let's stop it
                    self.reset_gesture(gest)
            self.ongoing_gestures_[gest] = True
            self.ongoing_num_ += 1
            self.start_times_[gest] = row['time'] # time start

            if self.verbose_:
                print("START: " + gest)
                print()

    def stop_gesture(self, gest, row):
        """
        finish the gesture, return the command
        """
        if self.checkTimeDelay(row['time']):
            if self.ongoing_gestures_[gest]: # check if the gestre was started in the first place
                if gest == 'pinch':
                    if row['time'] - self.start_times_['pinch'] > 100 and row['time'] - self.start_times_['pinch'] < 400:
                        # self.cmd_ = 'P'
                        self.recipient_ += 1
                        if self.recipient_ > self.numRecipients_:
                            self.recipient_ = 1
                        print("\nPINCH! Current Recipient: " + str(self.recipient_) + ", " + self.recipients_[self.recipient_] + "\n\n")
                        self.last_gesture_time_ = row['time']
                        self.reset_gesture(gest)
                        self.reset_all()
                        if self.verbose_:
                            print("STOP:  " + gest)
                    else:
                        self.reset_gesture(gest)

                elif row['time'] - self.start_times_[gest] > 200:
                    if gest == 'point':
                        self.doTheMath()
                    elif gest == 'fist':
                        self.cmd_ = 'F'
                    elif gest == 'snap':
                        self.cmd_ = 'S'
                    self.last_gesture_time_ = row['time']
                    self.reset_gesture(gest)
                    self.reset_all()
                    if self.verbose_:
                        print("STOP:  " + gest)
                else:
                    self.reset_gesture(gest)

    def processRow(self, row):
        """
        for each ongoing gesture, check that it's not timed out and for 'point' - add the row
        """
        if self.checkTimeDelay(row['time']):
            for gest in self.ongoing_gestures_.keys():  # for each gesture
                if self.ongoing_gestures_[gest]: # if it's still going
                    if not self.checkTimeOut(gest, row['time']): # check that we haven't timed out, and reset if we did
                        if gest == 'point': # if it's the pointing gesture - let's put it on the books
                            self.df_ = self.df_.append(row, ignore_index=True)

    def checkTimeDelay(self, time):
        """
        ensure that we are recording after 3 seconds has passed since the gesture start
        """
        return time - self.last_gesture_time_ > self.post_gesture_delay_

    def checkTimeOut(self, gest, time):
        """
        check if the gesture is out of time, end it
        """
        if time - self.start_times_[gest] > self.timeout_:
            print("GESTURE TIMED OUT")
            print()
            self.reset_gesture(gest)
            return True
        return False

    def doTheMath(self):
        """
        if the gesture is pointing, figure out what exactly it was, add the command
        """
        # do all the necessary pre processing on the dataframe to even get the gesture data
        self.processor_.run(self.df_)

        if self.learning_:
            self.gestureGen_.addGestureData(self.processor_.gest_)
        else:
            (gest_name, gest_cmd, gest_r2 ) = self.gestureList_.checkGestures(self.processor_.gest_)
            if gest_name == "none":
                # no gesture situation
                self.cmd_ = "none"
            else:
                # gesture is legit
                self.cmd_ = gest_cmd

    def isCmdPending(self):
        if self.cmd_ != 'none':
            return True
        return False

    def getCmd(self):
        return self.cmd_

    def resetCmd(self):
        self.cmd_ = "none"

# pre processing of a dataframe of rows into gesture data
class GestureProcessor(object):

    def __init__(self):
        # self.col_names_ = {'ex':[], 'ey':[], 'ez':[], 'p1':[], 'p2':[], 'p3':[], 'sys_cal':[], 'gyro_cal':[], 'time':[]}
        self.axis_ = ["ex", "ey", "ez"]
        self.df_ = None # data frame that contains all the good stuff
        self.thr_ = 5 # threshold; cut off anything that is below the threshold
        # self.init = {"ex": self.df.ex[0], "ey":self.df.ey[0], "ez": self.df.ez[0]} # initial angles; to be subtracted
        self.length_ = None #self.df.time.size # total length of the gesture
        # self.tlength = 0 if self.length == 0 else self.df.time[self.length-1] # total time length of the gesture
        self.gest_ = None

    def setThr(self, thr):
        """
        set the threshold
        """
        self.thr_ = thr

    def run(self, df):
        """
        get the data frame, run gesture processing on it
        """
        self.df_ = df
        self.length_ = self.df_.time.size # total length of the gesture
        self.gest_ = GestureData()
        self.dataPrep()

    def dataPrep(self):
        """
        Prepare all the data about the gesture and store it in the element of a gesture class
        """
        self.gest_.gyro_cal_ = self.df_['gyro_cal'].mean()
        if self.gest_.gyro_cal_ <= 2.5:
            print("WARNING: gyro calibration is low: " + str(self.gest_.gyro_cal_) )
        for col in self.axis_:
            # unfuck up the angles to ensure continious angle measurements
            for i in range(1, self.length_):
                if self.df_[col][i] - self.df_[col][i-1] < -180:
                    self.df_[col][i] += 360
                elif self.df_[col][i] - self.df_[col][i-1] > 180:
                    self.df_[col][i] -= 360
            a = self.df_[col].tolist() # transfer the column to an array

            a = [ x - a[0] for x in a ] # subtract the onset point
            self.gest_.amplitude_[col] = round(max( [ abs(x) for x in a ] ),3) #find the amplitude - max deviation from the onset point
            self.gest_.max_[col] = round(max(a),3) #find the max val
            self.gest_.min_[col] = round(min(a),3) #find the min val

            try:
                st_i = a.index(list(filter(lambda i: abs(i) > self.thr_, a))[0]) #index of first element outside threshold zone - start of gesture
                end_i = self.length_ - 1 - list(reversed(a)).index( list(filter(lambda i: abs(i) > self.thr_, list(reversed(a)) ))[0] ) #index of last element outside thresholds zone - end of gesture
                a = a[st_i: end_i] #gesture only
                a = [round(x / self.gest_.amplitude_[col], 3) for x in a] #scale by max element
                t_a = self.df_['time'].tolist()[st_i: end_i] #time array
                t_a = [x-t_a[0] for x in t_a] #shift it
                t_a =[x/max(t_a) for x in t_a] #scale it
                t_a = [round(x,2) for x in t_a] #time to two decimals
    #             plt.plot(x_a, a)
            except:
                print("OOOPS " + col)
                # what happened is that all elements are inside the treshold zone
                a = []
                t_a = []
            self.gest_.time_[col] = t_a
            self.gest_.euler_[col] = a
            self.gest_.lengths_[col] = len(t_a)

# true gesture against which geture data is compared
class Gesture(object):

    # file cosntructor
    def __init__(self, filename):
        def getName():
            line = getNextLine()
            try:
                if line[0] == 'gesture':
                    return (line[1], line[2])
                else:
                    raise ValueError('ERROR: gesture is not initialized properly')
            except:
                raise ValueError('ERROR: gesture is not initialized properly')
        def getNumConstraints():
            line = getNextLine()
            try:
                if line[0] == 'constraints':
                    return int(line[1])
                else:
                    raise ValueError('ERROR: constraints declaration is improper')
            except:
                raise ValueError('ERROR: overall constraints are not initialized properly')
        def getNextLine():
            line = f.readline().strip()
            while (len(line) == 0 or line[0] == "#"): # line is useless
                print(line)
                line = f.readline()
            return line.split()
        def createConstraint():
            line = getNextLine()
            try:
                if line[1] == "eq":
                    axis = getNextLine()[1]
                    coefs = [float(x) for x in getNextLine()[1][1:-1].split(',')]
                    r2_thr = float(getNextLine()[1])
                    rmse_thr = float(getNextLine()[1])
                    return Equation_Constraint(axis, coefs, r2_thr, rmse_thr)
                elif line[1] == "peak":
                    axis = getNextLine()[1]
                    peak = float(getNextLine()[1])
                    minmax = getNextLine()[1]
                    return Peak_Constraint(axis, peak, minmax)
            except:
                raise ValueError('ERROR: one of the constraints is not declared properly')
        self.constraints_ = []
        f = open(filename, 'r')
        (self.name_, self.cmd_) = getName()
        num = getNumConstraints()
        for i in range(num):
            self.constraints_ += [createConstraint()]

    def __str__(self):
        temp = "------------------ GESTURE " + self.name_ + "COMMAND " + self.cmd_ +  " ---------------------\n"
        temp += "number of constraints: " + str(len(self.constraints_)) + "\n"
        for i in range(len(self.constraints_)):
            temp += self.constraints_[i].__str__()
        temp += '------------------------------------------------\n'
        temp += "------------------ GESTURE " + self.name_ + " ---------------------\n\n\n"
        return temp

    def add_constraints(self, constraint):
        """
        add a constraint to the gesture.
        """
        self.constraints_ = self.constraints_.append(constraint)

    def check(self, gestureData):
        """
        check whether constraints fit;

        return: (True/False, R-squared)
        """
        r2 = -58
        for c in self.constraints_: # for each constraint
            if c.type_ == 'eq':
                r2 = c.check_constraint(gestureData)[1]
            if not c.check_constraint(gestureData)[0]:
                return (False, r2) # if not met - nope, doesn't fit
        return (True, r2)

#list of gestures - used to check the gestureData against all gestres.
class GestureList(object):

    def __init__(self):
        f = open("gestures.txt", 'r')
        self.gestures_ = []
        for i in range(int(f.readline().strip())):
            self.gestures_ += [Gesture(f.readline().strip())]

    def __str__(self):
        temp = "------------------ GESTURE LIST ---------------------\n"
        temp += "number of gestures: " + str(len(self.gestures_)) + "\n"
        for i in range(len(self.gestures_)):
            temp += self.gestures_[i].__str__()
        temp += '------------------------------------------------\n'
        temp += "------------------ GESTURELIST ---------------------\n\n\n"
        return temp

    def addGesture(self, gesture):
        self.gestures_ = self.gestures_.append(gesture)

    def checkGestures(self, gestureData):
        """
        check the incoming data against each gesture in the list, find its best match
        return (gest_name, gest_cmd, r2-score)
        """

        print("---------- Checking data against gestures ---------- \n")
        best_gesture = ("none", "", 0)
        for gesture in self.gestures_:
            check_res = gesture.check(gestureData)
            print("gesture\t" + gesture.name_ + "\tmet reqs\t" + str(check_res[0]) + "\tr2\t" + str(check_res[1]) + "\n")
            if check_res[0]: # if gesture was accepted
                if (best_gesture[0] == "none"): # it's the first one that matched
                    best_gesture = (gesture.name_, gesture.cmd_, check_res[1])
                else: # current gesture seems to be better than our last one, use the new one
                    if best_gesture[2] < check_res[1]:
                        best_gesture = (gesture.name_, gesture.cmd_, check_res[1])
        print("---------------------------------------------------- \n")
        print("best gesture is\t" + best_gesture[0] + "\tcmd\t" + best_gesture[1] + "\tr2\t" + str(best_gesture[2])  + "\n" )
        print("---------------------------------------------------- \n\n")
        return best_gesture

#constraint class; does nothing, but other constraints extend it
class Constraint(object):

    def __init__(self):
        pass


    def check_constraint(self):
        pass

# peak constraint: amplitude / max / min /rmse must be at least some value
class Peak_Constraint(Constraint):

    def __init__(self, axis, peak, minmax):
        self.axis_ = axis #which axis to check - ex, ey, ez
        self.peak_ = peak #value
        self.minmax_ = minmax # string: "max", "min", "amp"
        self.type_ = 'peak'

    def __str__(self):
        temp = "\n---------------  Peak Constraint ------------------\n"
        temp += "axis\t" + self.axis_ + "\t" + self.minmax_ + "\t" + str(self.peak_) + "\n"
        temp += "---------------------------------------------------\n\n"
        return temp

    def check_constraint(self, gestureData):
        # return pair (true/false, peak)
        if self.minmax_ == 'min': # must be less than smth
            return (gestureData.min_[self.axis] <= self.peak_, gestureData.min_[self.axis_])
        elif self.minmax_ == 'max': # must be mroe than smth
            return (gestureData.max_[self.axis] >= self.peak_, gestureData.max_[self.axis_])
        elif self.minmax_ == 'amp': # amplitude must eb more than something
            return (gestureData.amplitude_[self.axis_] <= self.peak_, [gestureData.amplitude_[self.axis_]] )

# equation constaint: check the r squared between experimental data and theoretical
class Equation_Constraint(Constraint):

    def __init__(self, axis, coefs, r2_threshold = 0.7, rmse_threshold = 1.0):
        self.axis_ = axis # axis to check equation for
        self.coefs_ = coefs # coefficients a0 a1 a2 a3 .. for polynomial a0 + a1*x + a2*x^2 + ...
        self.r2_thr_ = r2_threshold # threshold for the r2
        self.rmse_thr_ = rmse_threshold # threshold for the r2
        self.type_ = 'eq'

    def __str__(self):
        temp = "\n---------------  Equation Constraint ------------------\n"
        temp += "axis\t" + self.axis_ + "\t" + str(self.coefs_) + "\n"
        temp += "r2\t" + str(self.r2_thr_) + "\trmse\t" + str(self.rmse_thr_) + "\n"
        temp += "---------------------------------------------------\n\n"
        return temp

    def calc_poly_at(self, t):
        """
        calculate value of polynomial at time t
        """
        x = 0.0
        for i in range(len(self.coefs_)):
            x += self.coefs_[i] * pow(t, i)
        return x

    def calc_time_array(self, tarr):
        """
        calculate values for the entire time array
        """
        return [self.calc_poly_at(t) for t in tarr]

    def calc_r2_match(self, time, ground_truth):
        """
        calculate the r2 between the thoeretical time array and the epxerimentally acquired data
        """
        return r2_score(ground_truth, self.calc_time_array(time))

    def calc_rmse_match(self, time, ground_truth):
        """
        calculate the rmse between the thoeretical time array and the epxerimentally acquired data
        """
        return np.sqrt(mean_squared_error(ground_truth, self.calc_time_array(time)))

    #this will probably requrie other data type input - may be a data table or some maded up object
    def check_constraint(self, gestureData):
        """
        check if r2 value is below the threshold - contraint is satisfied
        """
        if len(gestureData.time_[self.axis_]) > 0:
            r2 = self.calc_r2_match( gestureData.time_[self.axis_], gestureData.euler_[self.axis_])
            rmse = self.calc_rmse_match( gestureData.time_[self.axis_], gestureData.euler_[self.axis_])
            return ( r2 > self.r2_thr_ and rmse < self.rmse_thr_, [r2, rmse] )
        else:
            return (False, [-57,-57])

# gesture data that comes out as a result of a completing a physical gesture. does not represent an actual gesture
class GestureData:
        def __init__(self):
            self.amplitude_ = {"ex":0.0, "ey": 0.0, "ez":0.0} # max deviation from onset point
            self.max_ = {"ex":0.0, "ey": 0.0, "ez":0.0} #maximum angle from onset point
            self.min_ = {"ex":0.0, "ey": 0.0, "ez":0.0} # minimum angle from onset point
            self.lengths_ = {"ex":0, "ey": 0, "ez":0} # length of each axis recording after processing
            self.time_ = {"ex":[], "ey": [], "ez":[]} # time arrays with time stamps; scaled, trimmed
            self.euler_ = {"ex":[], "ey": [], "ez":[]} # euler angle arrays; scaled, trimmed
            self.gyro_cal_ = 0 # gyroscope calibration - should be 3 all the time

        def __str__(self):
            temp = "---------- Gesture Data Info ---------- \n"
            temp += "gyro calibration:\t" + str(self.gyro_cal_) + "\n"
            temp += "amp:\t" + str(self.amplitude_) + "\n"
            temp += "max:\t" + str(self.max_) + "\n"
            temp += "min:\t" + str(self.min_) + "\n"
            temp += "leng:\t" + str(self.lengths_) + "\n"
            temp += "EX:\n" + str(self.time_['ex']) + "\n" + str(self.euler_['ex']) + "\n"
            temp += "EY:\n" + str(self.time_['ey']) + "\n" + str(self.euler_['ey']) + "\n"
            temp += "EZ:\n" + str(self.time_['ez']) + "\n" + str(self.euler_['ez']) + "\n"
            temp += "--------------------------------------- \n\n"
            return temp

# allows generating gestures! as a result, prints some stuff out, for us to take down, rethink, and ut into a txt file
class GestureGenerator(object):

    def __init__(self):
        self.axis_ = ['ex', 'ey', 'ez']
        self.count_ = {'ex': 0, 'ey': 0, 'ez': 0, 'total': 0 } # count the number of gestures circulating
        self.df_ = pd.DataFrame( {'time_ex': [], 'ex':[], 'time_ey':[], 'ey':[], 'time_ez':[], 'ez':[] } )
        # self.x_ = pd.DataFrame( {'time': [], 'ex': []} )
        # self.y_ = pd.DataFrame( {'time': [], 'ey': []} )
        # self.z_ = pd.DataFrame( {'time': [], 'ez': []} )
        self.amplitude_ = {"ex":[], "ey": [], "ez":[] } # max deviation from onset point
        self.max_ = {"ex":[], "ey": [], "ez":[]} #maximum angle from onset point
        self.min_ = {"ex":[], "ey": [], "ez":[]} # minimum angle from onset point

        # a + bc + cx^2
        self.coefs_ = {"ex": [0,0,0], "ey": [0,0,0], "ez": [0,0,0]} # resultant array with coefficients
        self.running_ = False # true false, whether or not we are recording

    def __str__(self):
        temp =  "\n\n-------------------------------------------\n"
        for ax in self.axis_:
            temp += "AXIS: " + ax + "\n"
            temp += "-------------------------------------------\n"
            temp += "count\t" + str(self.count_[ax]) + "\tnum_points\t" + str( len(self.df_[ax].dropna()) ) + "\n"
            temp += "max\tmean\t" + str(np.mean(self.max_[ax])) + "\tstd\t" + str(np.std(self.max_[ax])) + "\n"
            temp += "min\tmean\t" + str(np.mean(self.min_[ax])) + "\tstd\t" + str(np.std(self.min_[ax])) + "\n"
            temp += "amp\tmean\t" + str(np.mean(self.amplitude_[ax])) + "\tstd\t" + str(np.std(self.amplitude_[ax])) + "\n"
            temp += "-------------------------------------------\n"
            (r2,rmse) = self.testItselfAgainstModel(ax)
            temp += "r2\t" + str(r2) + "\trmse\t" + str(rmse) + "\tmean\t" + str( self.df_[ax].dropna().mean() ) + "\n"
            temp += "coefs\t" + str(np.array(self.coefs_[ax])) + "\n"
            temp += "-------------------------------------------\n"
        temp += "-------------------------------------------\n\n\n"
        return temp

    def record(self):
        """
        flip the whole recording rubbish
        """
        self.running_ = not self.running_

    def addGestureData(self, gestureData):
        """
        add a new gesture data point to our collection
        """
        self.count_['total'] += 1

        # for each axis let's do some magic
        print("-----------------------------------------")
        last_df_len = len(self.df_)
        for ax in self.axis_:
            if gestureData.lengths_[ax] > 10: # if the axis is too small, we're not gonna take it. The logic is that then it's probably just noise, low values
                # let's try it against our current model
                # (rmse, r2) = self.testSetAgainstModel(ax, gestureData)
                # print("new set, old model; ax:\t" + ax + "\tr2\t" + str(r2) + "\trmse\t" + str(rmse))
                # let's add that stuff to the list
                for i in range( gestureData.lengths_[ax] ):
                    self.df_ = self.df_.append( { ('time_'+ax): gestureData.time_[ax][i], ax: gestureData.euler_[ax][i] }, ignore_index = True )
                self.fit(ax)

        print("-----------------------------------------")
        inp = input("space - accept        s - save        r - reject\n")

        if inp == " ":
            # accept the gesture, add all other data
            for ax in self.axis_:
                if gestureData.lengths_[ax] > 10:
                    self.count_[ax] += 1
                    self.max_[ax] += [gestureData.max_[ax]]
                    self.min_[ax] += [gestureData.min_[ax]]
                    self.amplitude_[ax] += [gestureData.amplitude_[ax]]
            print(self.count_)
            print("-------------- gesture accepted -----------------\n")

        elif inp == "s":
            # accept and save the data!
            # the accept part
            for ax in self.axis_:
                if gestureData.lengths_[ax] > 10:
                    self.count_[ax] += 1
                    self.max_[ax] += [gestureData.max_[ax]]
                    self.min_[ax] += [gestureData.min_[ax]]
                    self.amplitude_[ax] += [gestureData.amplitude_[ax]]
            # the print part
            print(self.__str__())

        elif inp == "r":
            # reject the gesture, revert to last dataframe
            self.df_ = self.df_[0:last_df_len]
            self.count_['total'] -= 1
            print()
            for ax in self.axis_:
                if gestureData.lengths_[ax] > 10: # if the axis is too small, we're not gonna take it. The logic is that then it's probably just noise, low values
                    (r2, rmse) = self.testSetAgainstModel(ax, gestureData)
                    print("axis\t" + ax + "r2\t" + str(round(r2, 3)) + "\trmse\t" + str(round(rmse, 3)))
            print("-------------- gesture rejected and tested -----------------\n")





    def testSetAgainstModel(self, axis, gestureData):
        """
        fit the current model agaist given gesture data
        """
        x_exp = gestureData.time_[axis]
        y_exp = gestureData.euler_[axis]
        y_pred = [ ( self.coefs_[axis][0] + self.coefs_[axis][1]*x + self.coefs_[axis][2]*x*x) for x in x_exp ]
        rmse = np.sqrt(mean_squared_error(y_exp, y_pred))
        r2 = r2_score(y_exp, y_pred)
        return (round(r2,3), round(rmse,3) )

    def testItselfAgainstModel(self, axis):
        """
        fit the current model agaist itself
        """
        y = np.array(self.df_[['time_' + axis, axis]].dropna()[axis].tolist())
        y = y[:, np.newaxis]
        y_pred = [ ( self.coefs_[axis][0] + self.coefs_[axis][1]*x_ + self.coefs_[axis][2]*x_*x_) for x_ in np.array(self.df_[['time_' + axis, axis]].dropna()['time_' + axis].tolist()) ]
        rmse = np.sqrt(mean_squared_error(y, y_pred))
        r2 = r2_score(y, y_pred)
        return (round(r2,3), round(rmse,3))


    def fit(self, axis):
        """
        refit the model on the current set for given axis
        """
        #create lists cause we need them
        x = np.array(self.df_[['time_' + axis, axis]].dropna()['time_' + axis].tolist())
        y = np.array(self.df_[['time_' + axis, axis]].dropna()[axis].tolist())
        # reindex\print(x
        x = x[:, np.newaxis]
        y = y[:, np.newaxis]
        # let's make a 2nd degree fit
        polynomial_features= PolynomialFeatures(degree=2)
        # print(x)
        x_poly = polynomial_features.fit_transform(x)
        model = LinearRegression()
        model.fit(x_poly, y)
        # update the model
        self.coefs_[axis] = [model.intercept_[0], model.coef_[0][1], model.coef_[0][2]]
        y_pred = [ ( self.coefs_[axis][0] + self.coefs_[axis][1]*x_ + self.coefs_[axis][2]*x_*x_) for x_ in np.array(self.df_[['time_' + axis, axis]].dropna()['time_' + axis].tolist()) ]
        rmse = np.sqrt(mean_squared_error(y, y_pred))
        r2 = r2_score(y, y_pred)
        print("axis\t" + axis + "\tr2\t" + str(round(r2, 3)) + "\trmse\t" + str(round(rmse, 3)))

# data intake from arduino
class Global(object):

    def __init__(self):
        self.ser_ = serial.Serial('/dev/ttyUSB0', 115200) # set up serial communication
        self.row_prev_ = None
        self.row_now_ = None
        self.gestures_ = GestureSweeper()

        # radio rubbish
        self.msgID_ = 0 # message id
        self.myID_ = 0
        # self.recipient_ = 1
        # self.numRecipients_ = 1
        # self.recipients_ = {0:'glove', 1: 'wall'}

    def learn(self):
        self.gestures_.learn()

    def spin(self):
        """
        run the routines endlessly
        """
        # self.send_string('abc')
        while 1:
            # self.receive_string()
            self.get_row()
            if self.gestures_.isCmdPending():
                self.sendCommand(self.gestures_.getCmd())
                self.gestures_.resetCmd()
                # print(self.row_now_)
                # check_for_events()

    def sendCommand(self, command):
        """
        given a command, send to the arduino to get transmitted; use the current receipient
        """
        # create the message based on teh description below
        cmd = str(self.msgID_) + '_' + str(self.gestures_.recipient_) + '_' + str(self.myID_) + '_' + command + '_' + '0' + '!'
        cmd_encoded = cmd.encode() # encode it
        self.ser_.write(cmd_encoded) # send to arduino
        # self.ser_.write(cmd_encoded) # send to arduino again
        # self.ser_.write(cmd_encoded) # send to arduino and again just in case
        self.msgID_ += 1 # icrement the message id
        self.msgID_ %= 10 # loop it back
        print("------------COMMAND SENT: " + cmd + " --------------\n")


#  message_id   receiver_id   transmitter_id  command  acknowledgement stop_sign
#    1_1_0_s_a!

    def nextRecipient(self):
        """
        switch to next recipient
        """
        temp = self.recipient_
        self.recipient_ += 1
        if self.recipient_ > self.numRecipients_:
            self.recipient_ = 1
        print("--------------- RECIPIENT CHANGED from " + self.recipients_[temp] + " to " + self.recipients_[self.recipient_]  +  " ---------------\n")

    def send_string(self, str):
        """
        purely for testing purposes - send a particular message to arduino
        """
        str_encoded = str.encode()
        self.ser_.write(str_encoded)
        # self.ser_.write(b'7')
        print("sent message ")

    def receive_string(self):
        """
        purely for testing purposes  - receive a message from Arduino
        """
        if (self.ser_.in_waiting > 0):
            st = str(self.ser_.readline())
            st = st[3:-5]
            # if st[0] == 'e':
            print("received message " + st)
            self.send_string('abc')

    def get_row(self):
        """
        read a new row of data if available, get a string, invoke decode_row
        """
        if (self.ser_.in_waiting > 0):
            st = str(self.ser_.readline())
            st = st[3:-5]
            self.decode_row(st)

    def decode_row(self, st):
        """
        gets strings, puts them into a dictionary, checks for events
        """
        # st = "ex_init -0.22 ey_init -0.47 ez_init 0.82 sys_cal 3 gyro_cal 3 time 769 p1 0 p2 0 p3 1" #example row
        t = st.split()
        try:
            t = t[t.index('ex'):]
        except:
            print("error: ")
            print(t)
            return
        i = 0
        # print(t)
        row = {}
        while i < len(t):
            row[t[i]] = int(float(t[i+1])) if int(float(t[i+1])) == float(t[i+1]) else float(t[i+1])
            i+=2
        self.row_prev_ = self.row_now_
        self.row_now_ = row

        if self.row_now_['gyro_cal'] < 3:
            print("WARNING: gyro calibration incomplete")

        if self.row_prev_ != None:
            self.check_for_events()

    def check_for_events(self):
        """
        check for events and start and stop gesture accordingly
        """

        #peformaing pinching motion



        if self.row_now_['p1'] == 0 and self.row_now_['p2'] == 0 and not self.gestures_.running('pinch'): #touched three fingers together
            self.gestures_.start_gesture('pinch', self.row_now_)

        # blank and pinch is running
        if self.row_now_['p1'] == 1 and self.row_now_['p2'] == 1 and self.gestures_.running('pinch'): #touched three fingers together
            self.gestures_.stop_gesture('pinch', self.row_now_)

        # if pad 2 got grounded, meaning that thrid finger is touching the thumb
        if self.row_prev_['p2'] == 1 and self.row_now_['p2'] == 0: # first finger and third finger touched
            self.gestures_.start_gesture('snap', self.row_now_)
            self.gestures_.stop_gesture('fist', self.row_now_)
        # if pad 1 is grounded, meaning that middle and second finger are touched
        elif self.row_prev_['p1'] == 1 and self.row_now_['p1'] == 0: #touched the pointer and middle fingers
            self.gestures_.start_gesture('point', self.row_now_)
            self.gestures_.start_gesture('fist', self.row_now_)

        # if pad 1 is ungrounded, meaning that we stoped touching second and third fingers
        elif self.row_prev_['p1'] == 0 and self.row_now_['p1'] == 1: #untouched pointer and middle fingers
            self.gestures_.stop_gesture('point', self.row_now_)

        # pad 3 got grounded - third finger touched the palm
        elif self.row_prev_['p3'] == 1 and self.row_now_['p3'] == 0: # third finger and palm touched
            self.gestures_.stop_gesture('snap', self.row_now_)

        # no events occurred, we are just recording data (or not)
        else:
            self.gestures_.processRow(self.row_now_)









##
