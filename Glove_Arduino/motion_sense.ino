
void check_input(){
  if(Serial.available()){
    char in = Serial.read();
    if (in == ' ' && !listening){
      gesture_begin();
    }
    else if(in == ' ' && listening){
      gesture_end();
    }
  }
}


void gesture_begin(){
  recnum++;
  listening = true;
  rec_offsets();
  listening_start = millis();
}

void gesture_end(){
  listening = false;
  stoprec();
}

void rec(){
//  q = bno.getQuat();
//  q.normalize();
//  float temp = q.x();  q.x() = -q.y();  q.y() = temp;
//  q.z() = -q.z();
//  euler = q.toEuler();
//
////  imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
//  
//  g = bno.getVector(Adafruit_BNO055::VECTOR_GRAVITY);
  rec_vals();
  uint8_t system, gyro, accel, mag = 0; //system calibration variables
  bno.getCalibration(&system, &gyro, &accel, &mag);
  
//  Serial.print(-180/M_PI *euler.x()-offx); Serial.print(" ");
//  Serial.print(-180/M_PI *euler.y()-offy); Serial.print(" ");
//  Serial.print(-180/M_PI *euler.z()-offz); Serial.print(" ");

//  Serial.print(" ex_init "); Serial.print(euler.x()-offx);
//  Serial.print(" ey_init "); Serial.print(euler.y()-offy);
//  Serial.print(" ez_init "); Serial.print(euler.z()-offz);

  Serial.print(" ex "); Serial.print(euler.x()-offx);
  Serial.print(" ey "); Serial.print(euler.y()-offy);
  Serial.print(" ez "); Serial.print(euler.z()-offz);

  Serial.print(" p1 "); Serial.print(digitalRead(A0));
//  if (analogRead(A7) > 0)
//    Serial.print(1);
//  else
//    Serial.print(0);
  Serial.print(" p2 "); Serial.print(digitalRead(4));
  Serial.print(" p3 "); Serial.print(digitalRead(9));
//  Serial.print(g.x()-gx); Serial.print(" ");
//  Serial.print(g.y()-gy); Serial.print(" ");
//  Serial.print(g.z()-gz); Serial.print(" ");
//  Serial.print(offx); Serial.print(" ");
//  Serial.print(offy); Serial.print(" ");
//  Serial.print(offz); Serial.print(" ");
//  Serial.print(gx); Serial.print(" ");
//  Serial.print(gy); Serial.print(" ");
//  Serial.print(gz); Serial.print(" ");
//  Serial.print(sqrt(g.x()*g.x() + g.y()*g.y() + g.z()*g.z())); Serial.print(" ");
//
//  Serial.print(q.w()-qw); Serial.print(" ");
//  Serial.print(q.x()-qx); Serial.print(" ");
//  Serial.print(q.y()-qy); Serial.print(" ");
//  Serial.print(q.z()-qz); Serial.print(" ");
//  Serial.print(qw); Serial.print(" ");
//  Serial.print(qx); Serial.print(" ");
//  Serial.print(qy); Serial.print(" ");
//  Serial.print(qz); Serial.print(" ");
  
  Serial.print(" sys_cal "); Serial.print(system, DEC);
  Serial.print(" gyro_cal "); Serial.print(gyro, DEC); 
//  Serial.print(accel, DEC); Serial.print(" ");
//  Serial.print(mag, DEC); Serial.print(" ");
  Serial.print(" time "); Serial.println(millis()-listening_start);
//  Serial.println(recnum);
  delay(BNO055_SAMPLERATE_DELAY_MS);
}


void rec_offsets(){
//  q = bno.getQuat();
//  q.normalize();
//  float temp = q.x();  q.x() = -q.y();  q.y() = temp;
//  q.z() = -q.z();
//  euler = q.toEuler();
//  offx = -180/M_PI *euler.x(); offy = -180/M_PI *euler.y(); offz = -180/M_PI *euler.z();

//  imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
  
//  g = bno.getVector(Adafruit_BNO055::VECTOR_GRAVITY);
  rec_vals();
  offx = euler.x(); offy = euler.y(); offz = euler.z();
  gx = g.x(); gy = g.y(); gz = g.z();
  qw = q.w(); qx = q.x(); qy = q.y(); qz = q.z();
}

void stoprec(){
  listening = false;
}


void rec_vals(){
  q = bno.getQuat();
  quaternion_to_euler();
  g = bno.getVector(Adafruit_BNO055::VECTOR_GRAVITY);
}

void quaternion_to_euler(){
  float t1, t2;
  t1 = 2.0 * (q.w() * q.x() + q.y() * q.z());
  t2 = 1.0 - 2.0 * (q.x() * q.x() + q.y() * q.y());
  euler.x() = 180/M_PI *atan2(t1, t2);
  t2 = 2.0 * (q.w() * q.y() - q.z() * q.x());
  if (t2>1){t2 = 1;}
  if (t2<-1){t2 = -1;}
  euler.y() = 180/M_PI *asin(t2);
  t1 = 2.0 * (q.w() * q.z() + q.x() * q.y());
  t2 = 1.0 - 2.0 * (q.y()*q.y() + q.z()*q.z());
  euler.z() = 180/M_PI *atan2(t1, t2);
  if (euler.z() > 180){
    euler.z() = euler.z() - 360;
  }
}
