#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

/* Set the delay between fresh samples */
#define BNO055_SAMPLERATE_DELAY_MS (15)
//#define BNO055_SAMPLERATE_DELAY_MS (1000)

// Check I2C device address and correct line below (by default address is 0x29 or 0x28)
//                                   id, address
Adafruit_BNO055 bno = Adafruit_BNO055(-1, 0x28);


boolean listening = false;  
int recnum = 0;
float offx, offy, offz; //offset for euler's angles
float gx, gy, gz; //offset for g
float qw, qx, qy, qz; // offset for quaternions
unsigned long listening_start;

imu::Quaternion q;
imu::Vector<3> euler;
imu::Vector<3> g;




//-----------------------------------------
// transmitter rubbsish
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>


#define CE_PIN   7
#define CSN_PIN 8

const byte slaveAddress[5] = {'R','x','A','A','A'};


RF24 radio(CE_PIN, CSN_PIN); // Create a Radio

char dataToSend[10] = "Message 0";
char txNum = '0';


unsigned long currentMillis;
unsigned long prevMillis;
unsigned long txIntervalMillis = 1000; // send once per second

// transmitter rubbsish
//-----------------------------------------

void setup(void){
  pinMode(4, INPUT_PULLUP);
  pinMode(9, INPUT_PULLUP);
  pinMode(A0, INPUT_PULLUP);
  Serial.begin(115200);
  if(!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while(1);
  }

  delay(1000);
  bno.setExtCrystalUse(true);

  radio.begin();
  radio.setDataRate( RF24_250KBPS );
  radio.setPALevel(RF24_PA_HIGH);
  radio.setRetries(3,5); // delay, count
  radio.openWritingPipe(slaveAddress);
}

char inp[10];
int inp_i = 0;
int myID_ = 0;

void loop(void)
{
  rec();
  checkForInput();
}


void checkForInput(void ){
  /*
  check if input command is available. if not  - 
  */
  while(Serial.available()){         //From RPi to Arduino
    inp[inp_i] = static_cast<char>(Serial.read());  //conveting the value of chars to integer
    inp_i += 1;
    if (inp[inp_i-1] == '!'){
      send();
    }
    else if (inp_i == 10){
      Serial.println("WTFWTFWTFWTFWTFWTFWTFWTFWTF");
    }
  }
}






//====================

void send() {
    inp_i = 0;
    bool rslt;
    rslt = radio.write( &inp, sizeof(inp) );
//     Always use sizeof() as it gives the size as the number of bytes.
//     For example if dataToSend was an int sizeof() would correctly return 2
    int i = 1;
    while (i < 5){
      if (rslt) {
//        Serial.println("Acknowledge received");
        return;
      }
      else {
        rslt = radio.write( &inp, sizeof(inp) );
        i += 1;
      }  
    }
    Serial.println("TX failed");

}

//================








//
