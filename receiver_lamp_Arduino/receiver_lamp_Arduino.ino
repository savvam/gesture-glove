#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define CE_PIN  7
#define CSN_PIN 8

const byte thisSlaveAddress[5] = {'R','x','A','A','A'};

RF24 radio(CE_PIN, CSN_PIN);

char dataReceived[10]; // this must match dataToSend in the TX
bool newData = false;

//===========

void setup() {

    Serial.begin(115200);

    Serial.println("Lamp Starting");
    radio.begin();
    radio.setDataRate( RF24_250KBPS );
    radio.setPALevel(RF24_PA_LOW);
    radio.openReadingPipe(1, thisSlaveAddress);
    radio.startListening();

    pinMode(2, OUTPUT);
    digitalWrite(2, LOW);
}

//=============

void loop() {
    getData();
}

//==============

char last_msg_id = '9';
int myID = '1';

void getData() {
    if ( radio.available() ) {
        radio.read( &dataReceived, sizeof(dataReceived) );
        newData = true;
        showData();
        processData();
    }
}

void processData(void){
  if (dataReceived[0] != last_msg_id) {
    // it's not the same message as our last one
    last_msg_id = dataReceived[0];
    if (dataReceived[2] == myID) {
      //the message is for us, oh boy
      if (dataReceived[6] == 'F'){
        digitalWrite(2, LOW);
        Serial.println("LIGHTS ARE OFF");
      }
      else if (dataReceived[6] == 'S'){
        digitalWrite(2, HIGH);
        Serial.println("LIGHTS ARE ON");
      }
      else {
        Serial.println("this command doesnt apply to us");
      }
    }
    else{
      Serial.println("this message is not for us");
    }
  }
  else {
    Serial.println("hold up we already received this message");
  }
  for(int i = 0; i < 10; i++){
    dataReceived[i] = "";
  }
}



void showData() {
    if (newData == true) {
        Serial.print("Data received ");
        Serial.println(dataReceived);
        newData = false;
    }
}
