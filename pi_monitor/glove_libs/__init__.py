## message format:

#  message_id   receiver_id   transmitter_id  command  acknowledgement
#    1_1_0_s_a_
##

# S - snap
# F - fist
# P - pinch
# l - left
# r - right
# d - down
# u - up

import serial
import random
import string
from tkinter import *
import time
# import tkFont
from PIL import Image



# data intake from arduino
class Global(object):

    def __init__(self):
        self.ser_ = serial.Serial('/dev/ttyUSB0', 115200) # set up serial communication
        self.row_prev_ = None
        self.row_now_ = None

        # radio rubbish
        self.upright = True
        self.myID_ = '2'
        self.cmd_  = "r"
        self.cmd_last = 'r'
        self.newCmd_ = False
        self.move_ = 20
        self.cmd_names_ = {'r': 'right', 'l': 'left', 'u':'up', 'd':'down', 'S':'snap', 'F':'fist' }

        self.tk = Tk()
        self.tk.title = "Austin at EC"
        self.tk.resizable(0,0)
        self.tk.wm_attributes("-topmost", 1)

        self.canvas = Canvas(self.tk, width=1440, height=954, bd=0, highlightthickness=0)


        # self.id = self.canvas.create_oval(10, 10, 25, 25, fill="red")

        self.ec = PhotoImage(file='~/pi_monitor/glove_libs/ec.png')
        self.ec = self.ec.zoom(2, 2)
        self.ec_back = self.canvas.create_image(720,472,image=self.ec)

        self.austin = PhotoImage(file='~/pi_monitor/glove_libs/austin.png')
        self.austin_rev = PhotoImage(file='~/pi_monitor/glove_libs/ups.png')
        self.id = self.canvas.create_image(10, 10, image=self.austin)
        # self.canvas.move(self.id, 720, 472)
        self.canvas.move(self.id, 500, 400)
        # self.canvas.tag_raise(self.id)

        # self.background_label = Label(self.tk, image=self.ec)
        # self.background_label.place(x=0, y=0, relwidth=1, relheight=1)

        self.canvas.pack()
        self.tk.update_idletasks()
        self.tk.update()



    def spin(self):
        """
        run the routines endlessly
        """
        while 1:
            self.get_row()
            if self.newCmd_:
                self.processCmd()
            self.move()



    def get_row(self):
        """
        read a new row of data if available, get a string, invoke decode_row
        """
        if (self.ser_.in_waiting > 0):
            st = str(self.ser_.readline())
            # print(st)
            try:
                st = st[2:-3]
                if st[9] == '!' and st[2] == self.myID_:
                    self.cmd_last = self.cmd_
                    self.cmd_ = st[6]
                    self.newCmd_ = True
                print("received string: " + st)
            except:
                print("received string:___|" + st +"|___, error\n")

    def processCmd(self):
        print("--------------------------------------")
        print("Received command: " + self.cmd_ + ", which is " + self.cmd_names_[self.cmd_] )
        print("--------------------------------------\n")
        self.newCmd_ = False



    def move(self):
        if self.cmd_ == 'u':
            self.canvas.move(self.id, 0, -self.move_)
        elif self.cmd_ == 'd':
            self.canvas.move(self.id, 0, self.move_)
        elif self.cmd_ == 'l':
            self.canvas.move(self.id, -self.move_, 0)
        elif self.cmd_ == 'r':
            self.canvas.move(self.id, self.move_, 0)
            # self.cmd_ = 'S'
        elif self.cmd_ == 'S':
            x = self.canvas.coords(self.id)[0]
            y = self.canvas.coords(self.id)[1]
            self.ec_back = self.canvas.create_image(720,472,image=self.ec)
            if self.upright:
                self.id = self.canvas.create_image(10, 10, image=self.austin_rev)
            else:
                self.id = self.canvas.create_image(10, 10, image=self.austin)
            self.canvas.move(self.id, x, y)
            self.upright = not self.upright
            self.cmd_ = self.cmd_last
            # self.cmd_ = 'r'

        if self.canvas.coords(self.id)[0] > 1480:
            self.canvas.move(self.id, -1550, 0)
        if self.canvas.coords(self.id)[0] < -40:
            self.canvas.move(self.id, 1550,0)

        if self.canvas.coords(self.id)[1] > 970:
            self.canvas.move(self.id, 0, -1000)
        if self.canvas.coords(self.id)[1] < 0:
            self.canvas.move(self.id, 0, 1000)
        # self.cmd_ = ""
        self.tk.update_idletasks()
        self.tk.update()
        # time.sleep(1)

        # print(self.canvas.coords(self.id))




















##
